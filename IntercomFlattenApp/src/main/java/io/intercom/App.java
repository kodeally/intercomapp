package io.intercom;

import io.intercom.helpers.ArrayHelper;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Main application class to run IncomcomApp
 * @author ALAN
 */
public class App {

    /***
     * Simple main function to execute the program it takes no parameters uses a 
     * fix sample dataset [1,2,[3],4]
     * 
     */
    public static void main(String[] args) {
                
        List<Object> sourceData = Arrays.asList(1,2,Arrays.asList(3),4);
        System.out.println(String.format("Original dataset %s", sourceData));
        
        List<Integer> result = new LinkedList<>();
        ArrayHelper.flatten(sourceData, result);
        
        System.out.println(String.format("Flattened dataset %s", result));
    }
}
