package io.intercom.helpers;

import java.util.Iterator;
import java.util.List;

/***
 * ArrayHelper is a helper class to manage arrays.
 * @author ALAN
 */
public class ArrayHelper {

    /***
     * flatten take a given source array that contains integers and arrays and 
     * flattens it into a single integer array.
     * @param source array make up of integers and arrays
     * @param result array of integers
     */
    public static void flatten(List<Object> source, List<Integer> result) {

        if (source != null && source.size() > 0) {
            Iterator<Object> items = source.iterator();

            while (items.hasNext()) {
                Object item = items.next();
                if (item instanceof Integer) {
                    result.add((Integer)item);
                }
                else if (item instanceof List) {
                    flatten((List)item, result);
                }
                else {
                    throw new RuntimeException(String.format("Invalid item in the source list %s", item));
                }
            }
        }
    }
}
