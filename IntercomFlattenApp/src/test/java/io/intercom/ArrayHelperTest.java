package io.intercom;

import io.intercom.helpers.ArrayHelper;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
 
/**
 *
 * @author ALAN
 */
public class ArrayHelperTest {

    @Test
    public void arrayFlattenTest() {
        List<Object> sourceData = Arrays.asList(1,2,Arrays.asList(3),4);
        
        List<Integer> result = new LinkedList<>();
        ArrayHelper.flatten(sourceData, result);
        
        Arrays.asList(1,2,Arrays.asList(3),4);
        
        Assert.assertEquals(result, Arrays.asList(1,2,3,4));
    }
    
    @Test(expected = RuntimeException.class)
    public void arrayFlattenInvalidDataTest() {
        List<Object> sourceData = Arrays.asList(1,2,Arrays.asList(3), (float) 123.123);
        
        List<Integer> result = new LinkedList<>();
        ArrayHelper.flatten(sourceData, result);
        
        Assert.assertEquals(result, Arrays.asList(1,2,3,4));
    }
}
