package io.intercom;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author ALAN
 */
public class App {

    
    public static void main(String[] args) {
    
        try {
            CommandLineParameters parameters = new CommandLineParameters();
            if (parameters.parseCommandLine(args)) {
                System.out.println(String.format("Parse file %s looking for the top %d values.",parameters.getFilename(), parameters.getNumberCount()));

                TopNumbersFileParser parser = new TopNumbersFileParser();

                List<Integer> result = parser.findTopNumbers(parameters.getFilename(), parameters.getNumberCount());
                if (result != null) {
                    Iterator<Integer> items = result.iterator();
                    while (items.hasNext()) {
                        System.out.println(items.next());
                    }
                }
            }
        }
        catch(Exception exception) {
            System.out.println("");
        }
    }    
}
