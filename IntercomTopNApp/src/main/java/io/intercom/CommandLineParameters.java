package io.intercom;

import java.io.File;

/**
 *
 * @author ALAN
 */
public class CommandLineParameters {
    private String filename;
    private int topNumberCount;

    public String getFilename() {
        return filename;
    }

    public int getNumberCount() {
        return topNumberCount;
    }

    public boolean parseCommandLine(String[] args) {
        if (args.length == 2) {
            this.filename = args[0];
            File file = new File(this.filename);
            if (!file.exists()) {
                System.out.println(String.format("Invalid filename %s file does not exist.",this.filename));
                return false;
            }
            this.topNumberCount = Integer.parseInt(args[1]);
            if (this.topNumberCount == 0) {
                System.out.println("Invalid topN value need to be greater than 0.");
                return false;
            }
            return true;
        }
        else {
            System.out.println("Invalid parameters. Please specify a filename and a topN e.g data.txt 10");
        }
        return false;
    }
}
    
