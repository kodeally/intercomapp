package io.intercom;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Class to parse a file and get the top N largest values.
 * @author ALAN
 */
public class TopNumbersFileParser {
    
    /***
     * findTopNumbers takes a file and finds the highest numbers up to the numberCount specified.
     * @param fileName, file to parse
     * @param numberCount, the number of top integers to find.
     * @return list of top integers with a size of numberCount
     */
    public List<Integer> findTopNumbers(String fileName, int numberCount) {
        
        if (numberCount > 0) {   
            //Java 7 try-with-resources will close resources automatically
            try (Reader reader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(reader)) {

                PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(numberCount);

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    addLineToQueue(line, priorityQueue, numberCount);
                }

                return reverseQueue(priorityQueue);

            } catch(IOException exception) {
                throw new RuntimeException("Unexpected error failed to find top N numbers",exception);
            }
        }
        return null;
    }

    /***
     * This function adds the line value to the Queue if 
     * 1. The queue is smaller than the numberCount 
     * 2. The value is greater than the smallest value in the queue 
     * @param line, line value
     * @param priorityQueue, existing queue 
     * @param numberCount, the maximum number of elements the queue should have
     */
    private void addLineToQueue(String line, PriorityQueue<Integer> priorityQueue, int numberCount) {
        try {
            int value = Integer.valueOf(line);
            if (priorityQueue.size() < numberCount) {
                priorityQueue.add(value);
            }
            else {
                int smallestValueFromQueue = priorityQueue.peek();
                if (smallestValueFromQueue < value) {
                    priorityQueue.remove();
                    priorityQueue.add(value);
                }
            }
        } catch (NumberFormatException e) {
            System.out.println(String.format("Warn: Invalid line value %s",line));
        }
    }
    
    /***
     * Given a queue list this function will reverse it
     * @param list, list to reverse
     * @return, return sorted list 
     */
    private List<Integer> reverseQueue(PriorityQueue<Integer> list) {
        if (list != null) {
            int listSize = list.size();
            
            List<Integer> result = new ArrayList(listSize);
            for(int index = 0; index < listSize; index++) {
                result.add(list.remove());
            }
            Collections.reverse(result);
            return result;
        }
        
        return null;
    }
}