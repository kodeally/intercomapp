package io.intercom;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ALAN
 */
public class TopNumbersFileParserTest {

    @Test
    public void parseValidFileForTop2Numbers() {
        ClassLoader classLoader = getClass().getClassLoader();
	File file = new File(classLoader.getResource("data.txt").getFile());
        
        TopNumbersFileParser parser = new TopNumbersFileParser();
        List<Integer> result = parser.findTopNumbers(file.getAbsolutePath(), 2);
        
        Assert.assertEquals(result, Arrays.asList(123123, 79));
    }
}
